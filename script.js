// Try catch доречно використовувати у ситуація коли ми можемо очікувати помилку або в випадках отримання данних від API

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
]

const container = document.getElementById('root')
const checkerElems = ['author', 'name', 'price']

const checker = (elem) => {
    for (const value of checkerElems) {
        try {
            if (!elem[value]) {
                throw new Error(`${value} property is not found`);
            }
        } catch (error) {
            console.log(error.message);
            return false;
        }
    }
    return true;
}

const booksElems = (elems) => {
    const list = document.createElement('ul')
    list.textContent = 'Books'
    container.append(list)

    elems.forEach((elem) => {
        const items = document.createElement('li')
        for (const value of checkerElems) {
            const bookElem = document.createElement('div');
            bookElem.classList.add(value);
            bookElem.innerText = `${value} : ${elem[value]}`;

            items.append(bookElem);
            container.append(items);
        }

    })
}

const renderList = (books) => {
        const result = [];
        books.forEach((elem) => {
            if (checker(elem)) {
                result.push(elem);
            }
        });
        booksElems(result);
}

renderList(books)




